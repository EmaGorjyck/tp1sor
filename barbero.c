#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#define TRUE 1
#define FALSE 0
#define CHAIRS 10 //Sillas Disponibles
#define CANT_CUST 15 //Cantidad de Clientes
#define T_CUST 1 //Tiempo de llegada de Clientes
#define T_CORTE 1 //Tiempo de corte de pelo

sem_t barbero;
sem_t cliente;
pthread_mutex_t mi_mutex;

//Prototipo de funciones
void *f_cliente (void *);
void *f_barbero (void *);

int waiting=0;
int estadoBarbero = 1; //1 dormido, 0 despierto

//Main function
int main (void) {
    pthread_mutex_init ( &mi_mutex, NULL);

	sem_init(&barbero,0,0);
	sem_init(&cliente,0,1);
    pthread_t barb_t;
    pthread_t cust_t[CANT_CUST];

    pthread_create(&barb_t,NULL,f_barbero,NULL);

    for (int i=0;i<CANT_CUST;i++){
        sleep(T_CUST);
        pthread_create(&cust_t[i],NULL,f_cliente,NULL);
    }

    //pthread_join(barb_t,NULL);
    //pthread_exit(NULL);
    sem_destroy(&barbero);
    sem_destroy(&cliente);
    pthread_mutex_destroy(&mi_mutex);
    return(0);
}

void *f_cliente (void *n) {
    pthread_mutex_lock (&mi_mutex);
    if (waiting < CHAIRS) {
        waiting++;
        if(estadoBarbero==1){
            printf ("Ejem, ejem...,cof, cof...\n");
            estadoBarbero=0;
        }else{
            printf("Leyendo una revista\n");
            //sem_post (&barbero);
            sleep (T_CORTE);
        }
        sem_post (&cliente);
        pthread_mutex_unlock (&mi_mutex);
        sem_wait (&barbero);
    }else {
        printf ("Chau Chau\n");
        sleep (T_CORTE);
        pthread_mutex_unlock (&mi_mutex);
    }
}

void *f_barbero (void *j) { 
    while (TRUE) {
        sem_wait (&cliente);
        pthread_mutex_lock (&mi_mutex);
        if(waiting==0){
            estadoBarbero=1;
            printf("ZZZZZ...\n");
        }else{
            waiting--;
            printf ("XXXXX\n");
            sleep (T_CORTE);
        }
        sem_post (&barbero);
        pthread_mutex_unlock (&mi_mutex);
    }
}